<?php

class Supportchat extends Tools_Plugins_Abstract
{
    const TIN_TALK_ADDRESS = 'http://54.174.44.75:3000/';

    protected function _init()
    {
        $this->_layout = new Zend_Layout();
        $this->_layout->setLayoutPath(Zend_Layout::getMvcInstance()->getLayoutPath());

        if(($scriptPaths = Zend_Layout::getMvcInstance()->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->addScriptPath(__DIR__ . '/system/views/');
    }

    public function afterController()
    {
        $view = $this->_layout->getView();
        if(property_exists($view, 'pageData') && Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, self::TIN_TALK_ADDRESS);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($curl);
            curl_close($curl);
            if($response !== false) {
                $includeAngular = true;
                $scriptList = json_encode($this->_layout->getView()->headScript()->getContainer()->getArrayCopy());
                if(preg_match('/\/angular\.js/', $scriptList)) {
                    $includeAngular = false;
                }
                if($includeAngular) {
                    $this->_layout->getView()->headScript()
                        ->appendFile($this->_websiteUrl . 'plugins/supportchat/web/js/external/angular.js');
                }
                $clientInfo = '<input type="hidden" id="tinTalkSender" value="' . $this->_sessionHelper->getCurrentUser()->getFullName() . '"/>';
                $config = Application_Model_Mappers_ConfigMapper::getInstance()->getConfig();
                $userLogo = (isset($config['wicCorporateLogo'])) ? $config['wicCorporateLogo'] : 'system/images/noimage.png';
                $clientInfo .= '<input type="hidden" id="tinTalkSenderLogo" value="' . $this->_websiteUrl . $userLogo . '"/>';
                $uniqueId = md5($this->_sessionHelper->getCurrentUser()->getPassword() . $this->_sessionHelper->getCurrentUser()->getEmail());
                $clientInfo .= '<input type="hidden" id="tinTalkUniqueId" value="' . $uniqueId . '"/>';
                $content =  $view->content . $response . $clientInfo;
                $this->_response->setBody($content);
            }
        }
    }

    public function settingsAction()
    {
        $this->_layout->content = $this->_view->render('settings.phtml');
        echo $this->_layout->render();
    }
}
